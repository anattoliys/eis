package com.eis.taskManager.repositories;

import com.eis.taskManager.entities.Project;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectsRepository extends JpaRepository<Project, Long> {
    List<Project> findByNameLike(String name);
}
