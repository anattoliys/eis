package com.eis.taskManager.repositories;

import com.eis.taskManager.entities.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TasksRepository extends JpaRepository<Task, Long> {
    List<Task> findByNameLike(String name);

    List<Task> findByResponsibleId(Long id);

    List<Task> findByProjectId(Long id);
}
