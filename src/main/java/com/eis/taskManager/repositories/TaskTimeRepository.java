package com.eis.taskManager.repositories;

import com.eis.taskManager.entities.TaskTime;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskTimeRepository extends JpaRepository<TaskTime, Long> {
    List<TaskTime> findByTaskId(Long id);

    List<TaskTime> findByCreatorId(Long id);
}
