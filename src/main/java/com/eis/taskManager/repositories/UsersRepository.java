package com.eis.taskManager.repositories;

import com.eis.taskManager.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findByNameLike(String name);

    List<User> findAllByIdIn(List<Long> ids);
}
