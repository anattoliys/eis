package com.eis.taskManager.models;

public class TaskRequest {
    String name;
    String description;
    Boolean isActive;
    Long projectId;
    Long creatorId;
    Long responsibleId;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public Long getProjectId() {
        return projectId;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public Long getResponsibleId() {
        return responsibleId;
    }
}
