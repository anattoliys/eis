package com.eis.taskManager.models;

public class TaskTimeRequest {
    Long taskId;
    Long time;
    Long creatorId;
    String comment;

    public Long getTaskId() {
        return taskId;
    }

    public Long getTime() {
        return time;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public String getComment() {
        return comment;
    }
}
