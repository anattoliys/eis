package com.eis.taskManager.models;

public class UserRequest {
    String name;
    String surname;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
