package com.eis.taskManager.models;

import java.util.List;

public class ProjectRequest {
    String name;
    List<Long> users;

    public String getName() {
        return name;
    }

    public List<Long> getUsers() {
        return users;
    }
}
