package com.eis.taskManager.controllers;

import com.eis.taskManager.entities.User;
import com.eis.taskManager.models.UserRequest;
import com.eis.taskManager.services.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/users")
public class UsersController {
    private final UserService userService;

    public UsersController(UserService userService) {
        this.userService = userService;
    }

    // Добавить пользователя
    @PostMapping(path="/add")
    public @ResponseBody
    User add (@RequestBody UserRequest request) {
        return userService.add(request);
    }

    // Изменить пользователя
    @PutMapping(path="/{id}")
    public @ResponseBody
    User update (@PathVariable("id") Long id, @RequestBody UserRequest request) {
        return userService.update(id, request);
    }

    // Получить пользователя по id
    @GetMapping(path="/{id}")
    public @ResponseBody
    User get (@PathVariable("id") Long id) {
        return userService.get(id);
    }

    // Удалить пользователя
    @DeleteMapping(path="/{id}")
    public @ResponseBody
    void delete (@PathVariable("id") Long id) {
        userService.delete(id);
    }

    // Получить пользователя по имени
    @GetMapping(path="/find")
    public @ResponseBody
    List<User> find (@RequestParam String name) {
        return userService.find(name);
    }

    // Получить всех пользователей
    @GetMapping
    public @ResponseBody List<User> getAll() {
        return userService.getAll();
    }
}
