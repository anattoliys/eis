package com.eis.taskManager.controllers;

import com.eis.taskManager.entities.Project;
import com.eis.taskManager.models.ProjectRequest;
import com.eis.taskManager.services.ProjectService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/projects")
public class ProjectsController {
    private final ProjectService projectService;

    public ProjectsController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @PostMapping(path="/add")
    public @ResponseBody
    Project add (@RequestBody ProjectRequest request) {
        return projectService.add(request);
    }

    @PutMapping(path="/{id}")
    public @ResponseBody
    Project update (@PathVariable("id") Long id, @RequestBody ProjectRequest request) {
        return projectService.update(id, request);
    }

    @GetMapping(path="/{id}")
    public @ResponseBody
    Project get (@PathVariable("id") Long id) {
        return projectService.get(id);
    }

    @DeleteMapping(path="/{id}")
    public @ResponseBody
    void delete (@PathVariable("id") Long id) {
        projectService.delete(id);
    }

    @GetMapping(path="/find")
    public @ResponseBody
    List<Project> find (@RequestParam String name) {
        return projectService.find(name);
    }

    @GetMapping
    public @ResponseBody List<Project> getAll() {
        return projectService.getAll();
    }
}
