package com.eis.taskManager.controllers;

import com.eis.taskManager.entities.TaskTime;
import com.eis.taskManager.models.TaskTimeRequest;
import com.eis.taskManager.services.TaskTimeService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/taskTime")
public class TaskTimeController {
    private final TaskTimeService taskTimeService;

    public TaskTimeController(TaskTimeService taskTimeService) {
        this.taskTimeService = taskTimeService;
    }

    @PostMapping(path="/add")
    public @ResponseBody
    TaskTime add (@RequestBody TaskTimeRequest request) {
        return taskTimeService.add(request);
    }

    @PutMapping(path="/{id}")
    public @ResponseBody
    TaskTime update (@PathVariable("id") Long id, @RequestBody TaskTimeRequest request) {
        return taskTimeService.update(id, request);
    }

    @GetMapping(path="/{id}")
    public @ResponseBody
    TaskTime get (@PathVariable("id") Long id) {
        return taskTimeService.get(id);
    }

    @DeleteMapping(path="/{id}")
    public @ResponseBody
    void delete (@PathVariable("id") Long id) {
        taskTimeService.delete(id);
    }

    @GetMapping(path="/task/{id}")
    public @ResponseBody
    List<TaskTime> findByTaskId (@PathVariable("id") Long id) {
        return taskTimeService.findByTaskId(id);
    }

    @GetMapping(path="/creator/{id}")
    public @ResponseBody
    List<TaskTime> findByCreatorId (@PathVariable("id") Long id) {
        return taskTimeService.findByCreatorId(id);
    }

    @GetMapping
    public @ResponseBody List<TaskTime> getAll() {
        return taskTimeService.getAll();
    }
}
