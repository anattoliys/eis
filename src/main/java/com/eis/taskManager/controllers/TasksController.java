package com.eis.taskManager.controllers;

import com.eis.taskManager.entities.Task;
import com.eis.taskManager.models.TaskRequest;
import com.eis.taskManager.services.TaskService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/tasks")
public class TasksController {
    private final TaskService taskService;

    public TasksController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping(path="/add")
    public @ResponseBody
    Task add (@RequestBody TaskRequest request) {
        return taskService.add(request);
    }

    @PutMapping(path="/{id}")
    public @ResponseBody
    Task update (@PathVariable("id") Long id, @RequestBody TaskRequest request) {
        return taskService.update(id, request);
    }

    @GetMapping(path="/{id}")
    public @ResponseBody
    Task get (@PathVariable("id") Long id) {
        return taskService.get(id);
    }

    @DeleteMapping(path="/{id}")
    public @ResponseBody
    void delete (@PathVariable("id") Long id) {
        taskService.delete(id);
    }

    @GetMapping(path="/find")
    public @ResponseBody
    List<Task> findByName (@RequestParam String name) {
        return taskService.findByName(name);
    }

    @GetMapping(path="/responsible/{id}")
    public @ResponseBody
    List<Task> findByResponsibleId (@PathVariable("id") Long id) {
        return taskService.findByResponsibleId(id);
    }

    @GetMapping(path="/project/{id}")
    public @ResponseBody
    List<Task> findByProjectId (@PathVariable("id") Long id) {
        return taskService.findByProjectId(id);
    }

    @GetMapping
    public @ResponseBody List<Task> getAll() {
        return taskService.getAll();
    }
}
