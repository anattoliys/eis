package com.eis.taskManager.services;

import com.eis.taskManager.entities.Project;
import com.eis.taskManager.entities.User;
import com.eis.taskManager.models.ProjectRequest;
import com.eis.taskManager.repositories.ProjectsRepository;
import com.eis.taskManager.repositories.UsersRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService {
    private final ProjectsRepository projectRepository;
    private final UsersRepository userRepository;

    public ProjectService(ProjectsRepository projectRepository, UsersRepository userRepository) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }

    public Project add (ProjectRequest request) {
        Project project = new Project();

        return this.save(project, request);
    }

    public Project update (Long id, ProjectRequest request) {
        Project project = projectRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Project with id " + id + " not found"));

        return this.save(project, request);
    }

    private Project save(Project project, ProjectRequest request) {
        List<User> users = userRepository.findAllByIdIn(request.getUsers());

        project.setName(request.getName());
        project.setUsers(users);

        return projectRepository.save(project);
    }

    public Project get (Long id) {
        return projectRepository.findById(id).get();
    }

    public void delete (Long id) {
        projectRepository.deleteById(id);
    }

    public List<Project> find (String name) {
        return projectRepository.findByNameLike("%" + name + "%");
    }

    public List<Project> getAll() {
        return projectRepository.findAll();
    }
}
