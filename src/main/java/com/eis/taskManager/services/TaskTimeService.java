package com.eis.taskManager.services;

import com.eis.taskManager.entities.Task;
import com.eis.taskManager.entities.TaskTime;
import com.eis.taskManager.entities.User;
import com.eis.taskManager.models.TaskTimeRequest;
import com.eis.taskManager.repositories.TaskTimeRepository;
import com.eis.taskManager.repositories.TasksRepository;
import com.eis.taskManager.repositories.UsersRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TaskTimeService {
    private final TaskTimeRepository taskTimeRepository;
    private final TasksRepository taskRepository;
    private final UsersRepository userRepository;

    public TaskTimeService(TaskTimeRepository taskTimeRepository, TasksRepository taskRepository, UsersRepository userRepository) {
        this.taskTimeRepository = taskTimeRepository;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    public TaskTime add (TaskTimeRequest request) {
        TaskTime taskTime = new TaskTime();

        return this.save(taskTime, request);
    }

    public TaskTime update (Long id, TaskTimeRequest request) {
        TaskTime taskTime = taskTimeRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Task time with id " + id + " not found"));

        return this.save(taskTime, request);
    }

    private TaskTime save(TaskTime taskTime, TaskTimeRequest request) {
        Task task = taskRepository.findById(request.getTaskId()).orElseThrow(() -> new EntityNotFoundException("Task with id " + request.getTaskId() + " not found"));
        User creator = userRepository.findById(request.getCreatorId()).orElseThrow(() -> new EntityNotFoundException("User with id " + request.getCreatorId() + " not found"));

        taskTime.setTask(task);
        taskTime.setTime(request.getTime());
        taskTime.setCreator(creator);
        taskTime.setComment(request.getComment());

        return taskTimeRepository.save(taskTime);
    }

    public TaskTime get (Long id) {
        return taskTimeRepository.findById(id).get();
    }

    public void delete (Long id) {
        taskTimeRepository.deleteById(id);
    }

    public List<TaskTime> findByTaskId (Long id) {
        return taskTimeRepository.findByTaskId(id);
    }

    public List<TaskTime> findByCreatorId (Long id) {
        return taskTimeRepository.findByCreatorId(id);
    }

    public List<TaskTime> getAll() {
        return taskTimeRepository.findAll();
    }
}
