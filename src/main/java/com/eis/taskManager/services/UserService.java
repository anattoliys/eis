package com.eis.taskManager.services;

import com.eis.taskManager.entities.User;
import com.eis.taskManager.models.UserRequest;
import com.eis.taskManager.repositories.UsersRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class UserService {
    private final UsersRepository userRepository;

    public UserService(UsersRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User add (UserRequest request) {
        User user = new User();

        return this.save(user, request);
    }

    public User update (Long id, UserRequest request) {
        User user = userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("User with id " + id + " not found"));

        return this.save(user, request);
    }

    private User save(User user, UserRequest request) {
        user.setName(request.getName());
        user.setSurname(request.getSurname());

        return userRepository.save(user);
    }

    public User get (Long id) {
        return userRepository.findById(id).get();
    }

    public void delete (Long id) {
        userRepository.deleteById(id);
    }

    public List<User> find (String name) {
        return userRepository.findByNameLike("%" + name + "%");
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }
}
