package com.eis.taskManager.services;

import com.eis.taskManager.entities.Project;
import com.eis.taskManager.entities.Task;
import com.eis.taskManager.entities.User;
import com.eis.taskManager.models.TaskRequest;
import com.eis.taskManager.repositories.ProjectsRepository;
import com.eis.taskManager.repositories.TasksRepository;
import com.eis.taskManager.repositories.UsersRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TaskService {
    private final TasksRepository taskRepository;
    private final ProjectsRepository projectRepository;
    private final UsersRepository userRepository;

    public TaskService(TasksRepository taskRepository, ProjectsRepository projectRepository, UsersRepository userRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }

    public Task add (TaskRequest request) {
        Task task = new Task();

        return this.save(task, request);
    }

    public Task update (Long id, TaskRequest request) {
        Task task = taskRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Task with id " + id + " not found"));

        return this.save(task, request);
    }

    private Task save(Task task, TaskRequest request) {
        Project project = projectRepository.findById(request.getProjectId()).orElseThrow(() -> new EntityNotFoundException("Project with id " + request.getProjectId() + " not found"));
        User creator = userRepository.findById(request.getCreatorId()).orElseThrow(() -> new EntityNotFoundException("User with id " + request.getCreatorId() + " not found"));
        User responsible = userRepository.findById(request.getResponsibleId()).orElseThrow(() -> new EntityNotFoundException("User with id " + request.getResponsibleId() + " not found"));

        task.setName(request.getName());
        task.setDescription(request.getDescription());
        task.setIsActive(request.getIsActive());
        task.setProject(project);
        task.setCreator(creator);
        task.setResponsible(responsible);

        return taskRepository.save(task);
    }

    public Task get (Long id) {
        return taskRepository.findById(id).get();
    }

    public void delete (Long id) {
        taskRepository.deleteById(id);
    }

    public List<Task> findByName (String name) {
        return taskRepository.findByNameLike("%" + name + "%");
    }

    public List<Task> findByResponsibleId (Long id) {
        return taskRepository.findByResponsibleId(id);
    }

    public List<Task> findByProjectId (Long id) {
        return taskRepository.findByProjectId(id);
    }

    public List<Task> getAll() {
        return taskRepository.findAll();
    }
}
